﻿
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace TipCalculator
{
    public class TipModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public decimal Total { get; set; }

        public int Propina { get; set; }

        public int NoPersonas { get; set; }


        private decimal _totalPropina;
        public decimal TotalPropina {

            get { return _totalPropina; }

            set
            {
                _totalPropina = value;
                OnPropertyChanged();
            }
        }


        private decimal _totalConPropina;

        public decimal TotalConPropina {

            get { return _totalConPropina; }

            set
            {
                _totalConPropina = value;
                OnPropertyChanged();
            }

        }


        private decimal _propinaPorPersona;
        public decimal PropinaPorPersona {
            get { return _propinaPorPersona; }

            set
            {
                _propinaPorPersona = value;
                OnPropertyChanged();
            }

        }


        private decimal _totalPorPersona;
        public decimal TotalPorPersona {
            get { return _totalPorPersona; }
            set
            {
                _totalPorPersona = value;
                OnPropertyChanged();
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
