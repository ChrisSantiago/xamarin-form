﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;


namespace TipCalculator.ViewModel
{
    public class MainPageViewModel
    {
        public Command OperationsCommand { get; set; }

        public TipModel tipModel { get; set; }

        public MainPageViewModel()
        {
            OperationsCommand = new Command(doOperations);
            tipModel = new TipModel
            {
                Propina = 5,
                NoPersonas = 2

            };
        }

        private void doOperations()
        {
            if(tipModel.Total != 0 && tipModel.Propina != 0 && tipModel.NoPersonas != 0)
            {
                tipModel.TotalPropina = ((tipModel.Total * tipModel.Propina) / 100);

                tipModel.TotalConPropina = (tipModel.TotalPropina + tipModel.Total);

                tipModel.PropinaPorPersona = (tipModel.TotalPropina / tipModel.NoPersonas);

                tipModel.TotalPorPersona = ((tipModel.Total + tipModel.TotalPropina) / tipModel.NoPersonas);

            } else
            {
                App.Current.MainPage.DisplayAlert("Error de campos", "Por favor, verifique los campos", "OK");
            }

        }
    }
}
