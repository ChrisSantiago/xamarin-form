﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace EjemploLogin.Models
{
    public class LoginModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public string name;
        public string Name {
            get { return name; }

            set
            {
                name = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Name"));
            }
        }

        public string birthday;
        public string Birthday {
            get { return birthday; }
            set
            {
                birthday = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Birthday"));
            }
        }

        public ICommand SubmitCommand { get; set; }

        public LoginModel()
        {
            SubmitCommand = new Command(onSubmit);
        }

        public void onSubmit()
        {
            if(!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(Birthday))
            {
                MessagingCenter.Send(this, "LoginAlert", Name);
            } else
            {
                App.Current.MainPage.DisplayAlert("¡Opps...!", "Por favor, verifique los campos", "OK");
                
            }

        }
    }
}
