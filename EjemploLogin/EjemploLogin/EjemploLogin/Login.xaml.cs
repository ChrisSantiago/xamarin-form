﻿using EjemploLogin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EjemploLogin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public LoginModel loginModel;
        public Login()
        {
            InitializeComponent();
            this.loginModel = new LoginModel();
            MessagingCenter.Subscribe<LoginModel, string>(this, "LoginAlert", (sender, Name) =>{
                DisplayAlert("Titulo", Name, "OK");
            });
            this.BindingContext = loginModel;

            usernameEntry.Completed += (object sender, EventArgs e) =>
            {
                birthdayEntry.Focus();
            };

            birthdayEntry.Completed += (object sender, EventArgs e) =>
            {
                loginModel.SubmitCommand.Execute(null);
            };
        }
    }
}