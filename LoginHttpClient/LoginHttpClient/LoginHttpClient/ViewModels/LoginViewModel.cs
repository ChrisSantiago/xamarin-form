﻿using Acr.UserDialogs;
using LoginHttpClient.Models;
using LoginHttpClient.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LoginHttpClient.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public Command LonginCommand { get; set; }

        public LoginViewModel()
        {
            LonginCommand = new Command(async () => await (goPrivateSession()));
        }

        public async Task goPrivateSession()
        {
            try
            {
                IsBusy = true;
                var url = "https://demo2139500.mockable.io/user/login";
                var service = new HttpHelper<Response>();

                var loginResponse = await service.GetRestServiceDataAsync(url);
                Response response = loginResponse;
                IsBusy = false;
                UserDialogs.Instance.Toast(response.Message, TimeSpan.FromSeconds(2));
                //App.Current.MainPage.DisplayAlert("Mensaje", response.Message, "OK");
            }
            catch
            {
                IsBusy = false;
                await App.Current.MainPage.DisplayAlert("Mensaje", "Error", "OK");
            }
        }
    }
}
