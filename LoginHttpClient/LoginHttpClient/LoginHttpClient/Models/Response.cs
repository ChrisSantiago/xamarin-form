﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoginHttpClient.Models
{
    public class Response
    {
        public Boolean Status { get; set; }

        public int Code { get; set; }

        public String Message { get; set; }
    }
}
