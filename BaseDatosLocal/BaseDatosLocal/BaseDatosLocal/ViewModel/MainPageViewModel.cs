﻿using BaseDatosLocal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseDatosLocal.ViewModel
{
    public class MainPageViewModel
    {
        public List<Friend> Friends { get; set; }

        public MainPageViewModel()
        {
            FriendRepository repository = new FriendRepository();
            Friends = repository.GetAll().ToList();
        }
    }
}
