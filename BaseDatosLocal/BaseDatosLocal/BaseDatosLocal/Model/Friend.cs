﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseDatosLocal.Model
{
    public class Friend
    {
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public Friend(string firstName, string phone, string email)
        {
            this.FirstName = firstName;
            this.Phone = phone;
            this.Email = email;
        }

    }
}
