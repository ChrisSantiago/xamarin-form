﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseDatosLocal.Model
{
    public class FriendRepository
    {
        public IList<Friend> Frinds { get; set; }

        public FriendRepository()
        {
            Frinds = new List<Friend>();
            for (int i = 0; i < 10; i++)
            {
                Friend friend = new Friend("Amigo","(123)81235","amigo" + "hotmail.com");
                Frinds.Add(friend);
            }
        }

        public IList<Friend> GetAll()
        {
            return Frinds;
        }
    }
}
